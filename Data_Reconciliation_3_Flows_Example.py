#-------------------------------------------------------------------------------
# -*- #################
# Name:        Data_Reconciliation_3_Flows_Example.py
# Purpose:     Perform data reconciliation using scipy solver on a simple 
#			   example composed of three input flows and on output
#
# Author:      Antoine COLLET
#
# Updated:     04/08/2019
#-------------------------------------------------------------------------------

# 1) Load required modules
from scipy.optimize import minimize
import math

# 2) Raw data
#List containing the measured flowrates
Qv = [12,10,11,-31]
#List containing the errors on flowrates
EQv = [0.5,0.5,0.5,0.5]
#List containing the measured [U]
U= [52,51,67, 53]
#List containing the errors on [U]
EU= [1,1,1,1]
#List containing the measured [H3O+]
H3O = [0.011, 0.013, 0.010, 0.013]
#List containing the errors on [H3O+]
EH3O = [0.0001, 0.0001, 0.0001, 0.0001]

# 3) Objective function
def objective(vect_rec, vect_raw, EQv, EU, EH3O):
    SSE = 0
    dim = int(len(vect_rec)/3)
    for i in range (0, dim):
        SSE = SSE + math.pow((vect_rec[i] - vect_raw[i])/EQv[i],2)
        SSE = SSE + math.pow((vect_rec[i + dim] - vect_raw[i + dim])/EU[i],2)
        SSE = SSE + math.pow((vect_rec[i + dim * 2] - vect_raw[i + dim * 2])/EH3O[i],2)
    return SSE
#Test
vect_raw = [3,10,11,-32,52,51,67, 53, 0.011,0.013, 0.010, 0.013]
vect_rec = [12,10,11,-32,52,51,67, 53, 0.011,0.013, 0.010, 0.013]
objective(vect_rec , vect_raw, EQv, EU, EH3O)

# 4) Equality constraints: three equations

def e1(vect_rec):
    e1 = 0
    dim = int(len(vect_rec)/3)
    for i in range (0, dim):
        e1 = e1 + vect_rec[i]
    return e1

def e2(vect_rec):
    e2 = 0
    dim = int(len(vect_rec)/3)
    for i in range (0, dim):
        e2 = e2 + vect_rec[i] * vect_rec[i+4]
    return e2

def e3(vect_rec):
    e3 = 0
    dim = int(len(vect_rec)/3)
    for i in range (0, dim):
        e3 = e3 + vect_rec[i] * vect_rec[i+8]
    return e3

print(e1(vect_rec))
print(e2(vect_rec))
print(e3(vect_rec))

# 5) Solving the system
#Setting the constraints
cons = [{'type': 'eq', 'fun': e1},
        {'type': 'eq', 'fun': e2},
        {'type': 'eq', 'fun': e3}]

#Setting the initial set of values
vect_raw = Qv + U + H3O
#ex: vect_raw = [12,10,11,-31,52,51,67, 53, 0.011,0.013, 0.010, 0.013]

#Setting the bounds for each values of the set (here 200 is arbitrary)
bnds = [(0, 200) for i in range(len(vect_rec))]
#Only Flow Out sould be negative
bnds[int(len(vect_rec)/3)-1] = (-200, 0)
#Check the bounds
print(bnds)

#Solve the system and display the results
res = minimize(objective, method='SLSQP',x0=vect_raw, constraints=cons, args = (vect_raw, EQv, EU, EH3O), bounds=bnds)
print(res)

#Reconciliated vector
vect_rec = res.x
print(vect_rec)